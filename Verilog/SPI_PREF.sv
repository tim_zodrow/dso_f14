module SPI_PERF(clk, rst_n, cmd, wrt, SCLK, MISO, MOSI, SS_n, done, cmd_recieved);

// State Values
typedef enum reg[1:0] {WAIT, ACTIVE, BUFFER, DONE} state_t;
state_t state, nxt_state;

// Input/Output signals
input clk, rst_n, wrt, MISO;
input [15:0] cmd;
output reg done;
output SCLK, SS_n, MOSI;
output [15:0] cmd_recieved;

reg SS; // Slave Select
reg [3:0] sclk_pulse; // Counter for number of SCLK pulses
reg [4:0] sclk_counter; // Counter for SCLK
reg [15:0] shift_reg; // Shift Register to store cmd and shift in MISO

// State Machine Signals
logic sclk_pulse_tick, sclk_counter_tick, load, shift, nxt_SS, clr_counter;

// Output Assignments
assign SCLK = sclk_counter[4];
assign SS_n = ~SS;
assign MOSI = shift_reg[15];
assign cmd_recieved = shift_reg;

// FF for Register Values
always_ff @ (posedge clk, negedge rst_n)
	if(!rst_n)
		state <= WAIT;
	else
		state <= nxt_state;

always_ff @ (posedge clk, negedge rst_n)
	if(!rst_n)
		SS <= 0;
	else
		SS <= nxt_SS;

always_ff @ (posedge clk, negedge rst_n)
	if(!rst_n)
		sclk_pulse <= 4'h0;
	else if(sclk_pulse_tick)
		sclk_pulse <= sclk_pulse + 1;
	else
		sclk_pulse <= sclk_pulse;

always_ff @ (posedge clk, negedge rst_n)
	if(!rst_n)
		sclk_counter <= 5'b00000;
	else if(clr_counter)
		sclk_counter <= 5'b00000;
	else if(sclk_counter_tick)
		sclk_counter <= sclk_counter + 1;
	else
		sclk_counter <= sclk_counter;

always_ff @ (posedge clk, negedge rst_n)
	if(!rst_n)
		shift_reg <= 16'h0000;
	else if(load)
		shift_reg <= cmd;
	else if(shift)
		shift_reg <= {shift_reg[14:0], MISO};
	else
		shift_reg <= shift_reg;


// State Machine (states described above)
always @ (state, wrt, sclk_counter, sclk_pulse) begin
	// Initial/Default Values
	nxt_state = WAIT;
	nxt_SS = 0;
	sclk_pulse_tick = 0;
	sclk_counter_tick = 0;
	shift = 0;
	load = 0;
	done = 0;
	clr_counter = 0;
	
	case(state)
		WAIT: if(wrt) begin
			nxt_state = ACTIVE;
			nxt_SS = 1;
			load = 1;
		end
		ACTIVE: begin
			nxt_SS = 1;
			sclk_counter_tick = 1;
			nxt_state = ACTIVE;
			// Store value of MISO at Posedge SCLK
			if(&sclk_counter) begin
				shift = 1; // Shift at end of pulse
				sclk_pulse_tick = 1;
				if(&sclk_pulse)
					nxt_state = BUFFER;
			end
		end
		BUFFER: begin
			nxt_SS = 1;
			sclk_counter_tick = 1;
			// Counter for Delay
			if(&sclk_counter[1:0]) begin
				nxt_state = DONE;
				clr_counter = 1;
			end
			else
				nxt_state = BUFFER;
		end
		DONE: begin
			done = 1;
		end	
	endcase	
end
		
endmodule


