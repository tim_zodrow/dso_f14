module dig_core(clk,rst_n,adc_clk,trig1,trig2,SPI_data,wrt_SPI,SPI_done,ss,EEP_data,
                rclk,en,we,addr,ch1_rdata,ch2_rdata,ch3_rdata,cmd,cmd_rdy,clr_cmd_rdy,
				resp_data,send_resp,resp_sent);
				
  input clk,rst_n;								// clock and active low reset
  output adc_clk,rclk;							// 20MHz clocks to ADC and RAM
  input trig1,trig2;							// trigger inputs from AFE
  output [15:0] SPI_data;						// typically a config command to digital pots or EEPROM
  output wrt_SPI;								// control signal asserted for 1 clock to initiate SPI transaction
  output [2:0] ss;								// determines which Slave gets selected 000=>trig, 001-011=>chX_ss, 100=>EEP
  input SPI_done;								// asserted by SPI peripheral when finished transaction
  input [7:0] EEP_data;							// Formed from MISO from EEPROM.  only lower 8-bits needed from SPI periph
  output en,we;									// RAM block control signals (common to all 3 RAM blocks)
  output [8:0] addr;							// Address output to RAM blocks (common to all 3 RAM blocks)
  input [7:0] ch1_rdata,ch2_rdata,ch3_rdata;	// data inputs from RAM blocks
  input [23:0] cmd;								// 24-bit command from HOST
  input cmd_rdy;								// tell core command from HOST is valid
  output clr_cmd_rdy;
  output [7:0] resp_data;						// response byte to HOST
  output send_resp;								// control signal to UART comm block that initiates a response
  input resp_sent;								// input from UART comm block that indicates response finished sending
  
  //////////////////////////////////////////////////////////////////////////
  // Interconnects between modules...declare any wire types you need here//
  ////////////////////////////////////////////////////////////////////////
	wire dump_sel, cap_sel, trig_sel_uart, wrt_deci, wrt_trig_pos, wrt_trig_cfg, set_cap_done;
  wire [3:0] cmd_descimator;
  wire [7:0] cmd_trig_cfg, cmd_data;
  wire [8:0] cmd_addr, cmd_trig_pos, trace_end, addr_ptr;

  reg [3:0] descimator;
  reg [7:0] trig_cfg;
  reg [8:0] trig_pos;

  ////////////////////////
  // Output assignments //
  ////////////////////////
  assign adc_clk = ~rclk;
  assign resp_data = (trig_sel_uart) ? trig_cfg : cmd_data;

  ////////////////////////////////
  // Flops for resgister values //
  ////////////////////////////////
  always @ (posedge clk, negedge rst_n)
    if(!rst_n)
      descimator <= 4'h0;
    else if(wrt_deci)
      descimator <= cmd_descimator;

  always @ (posedge clk, negedge rst_n)
    if(!rst_n)
      trig_cfg <= 8'h00;
    else if(wrt_trig_cfg)
      trig_cfg <= cmd_trig_cfg;
    else if(set_cap_done)
      trig_cfg <= {trig_cfg[7:6], set_cap_done, trig_cfg[4:0]};

  always @ (posedge clk, negedge rst_n)
    if(!rst_n)
      trig_pos <= 9'h000;
    else if(wrt_trig_pos)
      trig_pos <= cmd_trig_pos;


  ///////////////////////////////////////////////////////
  // Instantiate the blocks of your digital core next //
  /////////////////////////////////////////////////////

	// Ram Interface Skeleton
	RAM_INTERFACE ri0(.clk(clk), .rst_n(rst_n), .RAM_en(en), .rclk(rclk), .dump_sel(dump_sel), .dump_addr(cmd_addr), .capture_sel(cap_sel), 
		      .capture_addr(addr_ptr), .RAM_addr(addr));

	// Command Module (Initial Commmands)
	cmd_module cm1(.clk(clk), .rst_n(rst_n), .cmd(cmd), .cmd_rdy(cmd_rdy), .wrt_SPI(wrt_SPI), .ss(ss), .SPI_data(SPI_data), .SPI_done(SPI_done), 
        	.resp_data(cmd_data), .send_resp(send_resp), .resp_sent(resp_sent), .clr_cmd_rdy(clr_cmd_rdy), .EEP_data(EEP_data), .ch1_rdata(ch1_rdata),
        	.ch2_rdata(ch2_rdata), .ch3_rdata(ch3_rdata), .ram_addr(cmd_addr), .trig_pos(cmd_trig_pos), .descimator(cmd_descimator), .trig_cfg(cmd_trig_cfg), 
        	.trace_end(trace_end), .wrt_trig_pos(wrt_trig_pos), .wrt_trig_cfg(wrt_trig_cfg), .wrt_deci(wrt_deci), .send_trig_uart(trig_sel_uart));

  // Add Capture Module (TODO hook up correct inputs/outputs)
  cptr_module cpt1(.clk(clk), .rst_n(rst_n), .rclk(rclk), .trig1(trig1), .trig2(trig2), .trig_cfg(trig_cfg), .trig_pos(trig_pos), .decim(descimator), 
          .trace_end(trace_end), .addr_ptr(addr_ptr), .we(we), .en(cap_sel), .set_cptr_done(set_cap_done));
  

endmodule
