`timescale 1ns/10ps
module DSO_dig(clk,rst_n,adc_clk,ch1_data,ch2_data,ch3_data,trig1,trig2,MOSI,MISO,
               SCLK,trig_ss_n,ch1_ss_n,ch2_ss_n,ch3_ss_n,EEP_ss_n,TX,RX, LED_n);
				
  input clk,rst_n;							        	// clock and active low reset
  output adc_clk;								          // 20MHz clocks to ADC
  input [7:0] ch1_data,ch2_data,ch3_data; // input data from ADC's
  input trig1,trig2;							        // trigger inputs from AFE
  input MISO;									            // Driven from SPI output of EEPROM chip
  output MOSI;									          // SPI output to digital pots and EEPROM chip
  output SCLK;									          // SPI clock (40MHz/16)
  output reg ch1_ss_n,ch2_ss_n,ch3_ss_n;  // SPI slave selects for configuring channel gains (active low)
  output reg trig_ss_n;								    // SPI slave select for configuring trigger level
  output reg EEP_ss_n;						        // Calibration EEPROM slave select
  output TX;									            // UART TX to HOST
  input RX;										            // UART RX from HOST
  output LED_n;								           	// control to active low LED
  
  ////////////////////////////////////////////////////
  // Define any wires needed for interconnect here //
  //////////////////////////////////////////////////
	wire clr_cmd_rdy, send_resp, resp_data, resp_sent, cmd_rdy, wrt_SPI, SPI_done, master_ss_n, en, we, addr;
	wire[2:0] ss;
	wire[15:0] SPI_data, EEP_data;
	wire[23:0] cmd;
  wire rclk;

	assign rclk = ~adc_clk;
  assign LED_n = 0;

  /////////////////////////////
  // Instantiate SPI master //
  ///////////////////////////

  SPI_PERF spi0(.clk(clk), .rst_n(rst_n), .cmd(SPI_data), .wrt(wrt_SPI), .SCLK(SCLK), .MISO(MISO), .MOSI(MOSI), .SS_n(master_ss_n), .done(SPI_done), 
      .cmd_recieved(EEP_data));
  
  ///////////////////////////////////////////////////////////////
  // You have a SPI master peripheral with a single SS output //
  // you might have to do something creative to generate the //
  // 5 individual SS needed (3 AFE, 1 Trigger, 1 EEP)       //
  ///////////////////////////////////////////////////////////
	always @ (*) begin  
		ch1_ss_n = 1;
		ch2_ss_n = 1;
		ch3_ss_n = 1;
		trig_ss_n = 1;
		EEP_ss_n = 1;
		case(ss)
			3'b000 : trig_ss_n = master_ss_n;
			3'b001 : ch1_ss_n = master_ss_n;
			3'b010 : ch2_ss_n = master_ss_n;
			3'b011 : ch3_ss_n = master_ss_n;
			default : EEP_ss_n = master_ss_n;
		endcase
	end

  ///////////////////////////////////
  // Instantiate UART_comm module //
  /////////////////////////////////
	UART_comm uc0(.clk(clk), .rst_n(rst_n), .RX(RX), .clr_cmd_rdy(clr_cmd_rdy), .trmt(send_resp), .tx_data(resp_data), .TX(TX), .tx_done(resp_sent), 
      .cmd(cmd), .cmd_rdy(cmd_rdy));
  ///////////////////////////
  // Instantiate dig_core //
  /////////////////////////
	dig_core dc0(.clk(clk), .rst_n(rst_n), .adc_clk(adc_clk), .trig1(trig1), .trig2(trig2), .SPI_data(SPI_data), .wrt_SPI(wrt_SPI), .SPI_done(SPI_done), 
      .ss(ss), .EEP_data(EEP_data[7:0]), .rclk(rclk), .en(en), .we(we), .addr(addr), .ch1_rdata(ch1_data), .ch2_rdata(ch2_data), .ch3_rdata(ch3_data),
      .cmd(cmd), .cmd_rdy(cmd_rdy), .clr_cmd_rdy(clr_cmd_rdy), .resp_data(resp_data), .send_resp(send_resp), .resp_sent(resp_sent));
  //////////////////////////////////////////////////////////////
  // Instantiate the 3 512 RAM blocks that store A2D samples //
  ////////////////////////////////////////////////////////////
  RAM512 iRAM1(.rclk(rclk),.en(en),.we(we),.addr(addr),.wdata(ch1_data),.rdata(ch1_rdata));
  RAM512 iRAM2(.rclk(rclk),.en(en),.we(we),.addr(addr),.wdata(ch2_data),.rdata(ch2_rdata));
  RAM512 iRAM3(.rclk(rclk),.en(en),.we(we),.addr(addr),.wdata(ch3_data),.rdata(ch3_rdata));

endmodule
  
