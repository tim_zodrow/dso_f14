module RAM_INTERFACE(clk, rst_n, RAM_en, rclk, dump_sel, dump_addr, capture_sel, capture_addr, RAM_addr);

input clk, rst_n, dump_sel, capture_sel;
input [8:0] dump_addr, capture_addr;

output RAM_en, rclk;
output reg [8:0] RAM_addr;

reg [1:0] rclk_counter;

assign rclk = rclk_counter[1];
assign RAM_en = capture_sel | dump_sel;

always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		RAM_addr <= 9'h000;
	else if(capture_sel)
		RAM_addr <= capture_addr;
	else if(dump_sel)
		RAM_addr <= dump_addr;
	else
		RAM_addr <= RAM_addr;
end

always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		rclk_counter <= 2'b00;
	else
		rclk_counter <= rclk_counter + 1;
end

endmodule
