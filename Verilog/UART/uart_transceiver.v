module uart_transceiver(clk, rst_n, trmt, tx_data, tx_done, TX, rdy, rx_data, clr_rdy, RX);

// Input/Output
input clk, rst_n, trmt, clr_rdy, RX;
input [7:0] tx_data;
output tx_done, rdy, TX;
output [7:0] rx_data;

// Implement TX and RX
UART_tx utx0(.clk(clk), .rst_n(rst_n), .tx_data(tx_data), .trmt(trmt), .TX(TX), .tx_done(tx_done));
UART_rx urx0(.clk(clk), .rst_n(rst_n), .RX(RX), .clr_rdy(clr_rdy), .rx_data(rx_data), .rdy(rdy));

endmodule
