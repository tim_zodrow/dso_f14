module UART_comm(clk, rst_n, RX, clr_cmd_rdy, trmt, tx_data, TX, tx_done, cmd, cmd_rdy);

input clk, rst_n, RX, clr_cmd_rdy, trmt;
input [7:0] tx_data;

output TX, tx_done;
output reg cmd_rdy;
output [23:0] cmd;

typedef enum {IDLE, RCV} state_t;
state_t state, nxt_state;

reg cmd_rdy_nxt;
reg [1:0] cmd_cnt, cmd_cnt_nxt;
reg state_this;
reg [7:0] high_cmd, high_cmd_nxt, mid_cmd, mid_cmd_nxt;

wire rdy;
wire [7:0] rx_data;

logic add_cmd_cnt, clr_rdy, clr_cmd_cnt;

uart_transceiver u0(.clk(clk), .rst_n(rst_n), .trmt(trmt), .tx_data(tx_data), .TX(TX), 
			 					.rdy(rdy), .rx_data(rx_data), .clr_rdy(clr_rdy), .RX(RX));

//Flop for state, high_cmd and low_cmd
always_ff @(posedge clk, negedge rst_n) begin
	if(!rst_n) begin
		state <= IDLE;
		high_cmd <= 8'h00;
		mid_cmd <= 8'h00;
	end else begin
		state <= nxt_state;
		high_cmd <= high_cmd_nxt;
		mid_cmd <= mid_cmd_nxt;
	end
end

//Flop for keeping track of what bits within cmd are sent
always_ff @(posedge clk, negedge rst_n) begin
	if(!rst_n) 
		cmd_cnt <= 2'b00;
	else if (clr_cmd_cnt)
		cmd_cnt <= 2'b00;
	else if (add_cmd_cnt)
		cmd_cnt <= (cmd_cnt + 1);
end

//Flop for cmd_rdy
always_ff @(posedge clk, negedge rst_n) begin
	if(!rst_n) 
		cmd_rdy <= 1'b0;
	else if (clr_cmd_rdy)
		cmd_rdy <= 1'b0;
	else
		cmd_rdy <= cmd_rdy_nxt;
end

always_comb begin
	//Default outputs
	nxt_state = IDLE;
	high_cmd_nxt = high_cmd;
	mid_cmd_nxt = mid_cmd;
	cmd_rdy_nxt = 1'b1; //cmd_rdy asserted until receiving data
	add_cmd_cnt = 1'b0;
	clr_cmd_cnt = 1'b0;
	clr_rdy = 1'b0;

	case(state)
		IDLE: begin
			if(!rdy) begin //start receiving when rdy
				nxt_state = RCV;
				clr_rdy = 1'b1;
				cmd_rdy_nxt = 1'b0;
			end
			if(cmd_cnt != 2'b00)
				cmd_rdy_nxt = 1'b0;
		end
		RCV: begin
			cmd_rdy_nxt = 1'b0;
			if(rdy) begin
				if(cmd_cnt == 2'b00) begin //getting first byte (high bits)
					high_cmd_nxt = rx_data; //set high bits
					add_cmd_cnt = 1'b1;     //add to cmd_cnt
				end
				if(cmd_cnt == 2'b01) begin //getting second byte (mid bits)
					mid_cmd_nxt = rx_data;  //set mid bits
					add_cmd_cnt = 1'b1;     //add to cmd_cnt
				end
				if(cmd_cnt == 2'b10) begin //done, current rx_data = low bits
					clr_cmd_cnt = 1'b1;     //clear cmd_cnt
					cmd_rdy_nxt = 1'b1;     //assert cmd_rdy
			  end
		  end
			else
				nxt_state = RCV; //stay in receive if not ready
		end			
	endcase
end

//cmd is set to concatination of high, mid and current bits
assign cmd = {high_cmd, mid_cmd, rx_data};

endmodule
