module UART_tx(clk, rst_n, tx_data, trmt, TX, tx_done);

input clk, rst_n, trmt;
input [7:0] tx_data;
output tx_done, TX;

typedef enum reg {IDLE,TRANS} state_t;
state_t state, nxt_state;

reg [5:0] baud_cnt;
reg [3:0] bit_cnt;
reg [9:0] tx_shift_reg;

logic load, trnsmt, shift, rst_bit_cnt, rst_baud_cnt, tx_done;

//////////////
//State flop//
//////////////
always_ff @(posedge clk, negedge rst_n) begin
	if (!rst_n)
		state <= IDLE;
	else
		state <= nxt_state;
end

////////////////////////////////////////
//Flop for shift reg whos LSB forms TX//
////////////////////////////////////////
always_ff @(posedge clk, negedge rst_n) begin
	if (!rst_n)
		tx_shift_reg <= 10'h000; //reset reg
	else if (load)
		tx_shift_reg <= {1'b1,tx_data,1'b0}; //load signal
  else if (shift)
		tx_shift_reg <= {1'b1,tx_shift_reg[9:1]}; //shift signal out to right
end

//Assign TX to LSB of tx shift reg//
assign TX = tx_shift_reg[0];

////////////////////////
//Flop for bit counter//
////////////////////////
always_ff @(posedge clk) begin
	if (rst_bit_cnt)
		bit_cnt <= 4'b0000;
	else if (shift)
		bit_cnt <= (bit_cnt + 1);
end

/////////////////////////
//Flop for baud (shift)//
/////////////////////////
always_ff @(posedge clk) begin
	if (rst_baud_cnt)
		baud_cnt <= 6'b000000;
	else
		baud_cnt <= (baud_cnt + 1);
end

//shift when baud cnt = 43
assign shift = (baud_cnt == 6'b101001) ? 1'b1 : 1'b0; 

///////////////////////////
//SM that controls output//
///////////////////////////
always_comb begin
	//Default Outputs//
	load = 0;
	rst_baud_cnt = 0;
	rst_bit_cnt = 0;
	tx_done = 1'b1;
	nxt_state = IDLE;

	case (state)
		IDLE: begin
			rst_baud_cnt = 1; //reset both counters
			rst_bit_cnt = 1;
			if (trmt) begin   //trmt initiates trans
				load = 1;
				nxt_state = TRANS;
			end else
				nxt_state = IDLE;
		end
		TRANS: begin //collecting 16-bit packet
			tx_done = 1'b0;
			if (shift)         //reset baud cnt after every shift
				rst_baud_cnt = 1;
			if (bit_cnt==4'hA) //done after 10 bits
				nxt_state = IDLE;
			else
				nxt_state = TRANS;
		end
	endcase
end

endmodule

