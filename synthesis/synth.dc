###################################
# Read in the verilog files first #
###################################
read_file -format sverilog { ./../Verlilog/raw2corrected.v,\
								./../Verilog/cptr_module.sv,\
								./../Verilog/cmd_module.sv,\
								./../Verilog/Given_Files/dig_core.v,\
								./../Verilog/Given_Files/DSO_dig.v,\
								./../Verilog/RAM_INTERFACE.sv,\
								./../Verilog/SPI_PREF.sv,\
								./../Verilog/trigger_logic.sv,\
								./../Verilog/UART/uart_comm.sv,\
								./../Verilog/UART/uart_transceiver.sv,\
								./../Verilog/UART/uart_tx.sv,\
								./../Verilog/UART/uart_rx.sv,\
								./../Verilog/Given_Files/RAM512_shell}

###################################
# Set Current Design to top level #
###################################
set current_design DSO_dig

##############################
# Constrain and assign clock #
##############################
create_clock -name "clk" -period 2.5 -waveform {0 1} {clk}
set_dont_touch_network [find port clk]

##################################
#Constrain input timings & Drive #
##################################
set prim_inputs [remove_from_collection [all_inputs] [find port clk]]
set_input_delay -clock clk 0.5 $prim_inputs
set_driving_cell -lib_cell ND2D2BWP -from_pin A1 -library tcbn40lpbwptc $prim_inputs
set_drive 0.1 rst_n
# set_drive 3 [find port Bp]

###################################
# Constrain output timing & loads #
###################################
set_output_delay -clock clk 0.5 [all_outputs]
set_load 0.1 [all_outputs]

##################################
# Set wireload & transition time #
##################################
set_wire_load_model -name TSMC32K_Lowk_Conservative -library tcbn40lpbwptc
set_max_transition 0.15 [current_design]

#########################################
# Set clock uncertainty and do fix hold #
#########################################
set_clock_uncertainty 0.15 clk
set_fix_hold clk

##################################
# Ungroup and flatten hierarchy #
################################
ungroup -all -flatten

######################
# Compile the design #
######################
compile -map_effort medium

##################################
# Generate timing & Area reports #
##################################
report_timing -delay max > max_delay.rpt
report_timing -delay min > min_delay.rpt
report_area > area.rpt

###########################################
# Write out resulting synthesized netlist #
###########################################
write -format verilog DSO_dig -output DSO_dig.vg
remove_design -all
exit
