`timescale 1ns/10ps
module DSO_dig_tb();
	
reg clk,rst_n;							// clock and reset are generated in TB

reg [23:0] cmd_snd;						// command Host is sending to DUT
reg send_cmd;
reg clr_resp_rdy;

wire adc_clk,MOSI,SCLK,trig_ss_n,ch1_ss_n,ch2_ss_n,ch3_ss_n,EEP_ss_n;
wire TX,RX;

wire [15:0] cmd_ch1,cmd_ch2,cmd_ch3;			// received commands to digital Pots that control channel gain
wire [15:0] cmd_trig;							// received command to digital Pot that controls trigger level
wire cmd_sent,resp_rdy;							// outputs from master UART
wire [7:0] resp_rcv;
wire [7:0] ch1_data,ch2_data,ch3_data;
wire trig1,trig2;

//////////////////////////////
// Memory for checking dump //
//////////////////////////////
reg [7:0]mem_ch1[0:511];
reg [7:0]mem_ch2[0:511];
reg [7:0]mem_ch3[0:511];
reg [10:0] i;

///////////////////////////
// Define command bytes //
/////////////////////////
localparam DUMP_CH  = 8'h01;		// Channel to dump specified in low 2-bits of second byte
localparam CFG_GAIN = 8'h02;		// Gain setting in bits [4:2], and channel in [1:0] of 2nd byte
localparam TRIG_LVL = 8'h03;		// Set trigger level, lower byte specifies value (46,201) is valid
localparam TRIG_POS = 8'h04;		// Set the trigger position. This is a 13-bit number, samples after capture
localparam SET_DEC  = 8'h05;		// Set decimator, lower nibble of 3rd byte. 2^this value is decimator
localparam TRIG_CFG = 8'h06;		// Write trig config.  2nd byte 00dettcc.  d=done, e=edge,
localparam TRIG_RD  = 8'h07;		// Read trig config register
localparam EEP_WRT  = 8'h08;		// Write calibration EEP, 2nd byte is address, 3rd byte is data
localparam EEP_RD   = 8'h09;		// Read calibration EEP, 2nd byte specifies address

//////////////////////
// Instantiate DUT //
////////////////////
DSO_dig iDUT(.clk(clk),.rst_n(rst_n),.adc_clk(adc_clk),.ch1_data(ch1_data),.ch2_data(ch2_data),
             .ch3_data(ch3_data),.trig1(trig1),.trig2(trig2),.MOSI(MOSI),.MISO(MISO),.SCLK(SCLK),
             .trig_ss_n(trig_ss_n),.ch1_ss_n(ch1_ss_n),.ch2_ss_n(ch2_ss_n),.ch3_ss_n(ch3_ss_n),
			 .EEP_ss_n(EEP_ss_n),.TX(TX),.RX(RX),.LED_n());
			   
///////////////////////////////////////////////
// Instantiate Analog Front End & A2D Model //
/////////////////////////////////////////////
AFE_A2D iAFE(.clk(clk),.rst_n(rst_n),.adc_clk(adc_clk),.ch1_ss_n(ch1_ss_n),.ch2_ss_n(ch2_ss_n),.ch3_ss_n(ch3_ss_n),
             .trig_ss_n(trig_ss_n),.MOSI(MOSI),.SCLK(SCLK),.trig1(trig1),.trig2(trig2),.ch1_data(ch1_data),
			 .ch2_data(ch2_data),.ch3_data(ch3_data));
			 
/////////////////////////////////////////////
// Instantiate UART Master (acts as host) //
///////////////////////////////////////////
UART_comm_mstr iMSTR(.clk(clk), .rst_n(rst_n), .RX(TX), .TX(RX), .cmd(cmd_snd), .send_cmd(send_cmd),
                     .cmd_sent(cmd_sent), .resp_rdy(resp_rdy), .resp_rcv(resp_rcv), .clr_resp_rdy(clr_resp_rdy));

/////////////////////////////////////
// Instantiate Calibration EEPROM //
///////////////////////////////////
SPI_EEP iEEP(.clk(clk),.rst_n(rst_n),.SS_n(EEP_ss_n),.SCLK(SCLK),.MOSI(MOSI),.MISO(MISO));

always
  #5 clk = ~clk;
	
initial begin
  clk = 0;
  rst_n = 0;	
  #100;		// assert reset
  ///////////////////////////////
  // Your testing goes here!! //
  send_cmd = 0;
  clr_resp_rdy = 0;
  #200;
  repeat(2) @(posedge clk);
  rst_n = 1;  // deassert reset ... ready to start testing!
	repeat(10) @ (posedge clk);
  

      /////////////////////////////////////////////////////////////
     /////////////////////////////////////////////////////////////
    // Configure analog gain of all channels to 101 (1 Volt) ////
   /////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////

  ///////////////
  // Channel 1 //
  //////////////
  cmd_snd = 24'h021C00;
  send_cmd = 1;

  @ (posedge clk); // wait a bit

  send_cmd = 0;
  clr_resp_rdy = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  // Check to make sure that gain was correctly set to 1 volts
  if (iAFE.POT1.iCHX_CFG.cmd_rcvd == 16'h13DD)
    $display("Channel 1 gain sucessfully set to 1 volts!");
  else
    $display("Channel 1 gain NOT sucessful");


  ///////////////
  // Channel 2 //
  //////////////
  send_cmd = 1;
  cmd_snd = 24'h021D00;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  // Check to make sure that gain was correctly set to 1 volts
  if (iAFE.POT2.iCHX_CFG.cmd_rcvd == 16'h13DD)
    $display("Channel 2 gain sucessfully set to 1 volts!");
  else
    $display("Channel 2 gain NOT sucessful");


  ///////////////
  // Channel 3 //
  //////////////
  send_cmd = 1;
  cmd_snd = 24'h021E00;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  // Check to make sure that gain was correctly set to 1 volts
  if (iAFE.POT3.iCHX_CFG.cmd_rcvd == 16'h13DD)
    $display("Channel 3 gain sucessfully set to 1 volts!");
  else
    $display("Channel 3 gain NOT sucessful");


  ////////////////////////////////////////////////
  // Set trigger level to 0x64 (100 in decimal) //
  ///////////////////////////////////////////////

  send_cmd = 1;
  cmd_snd = 24'h030064;

  @(posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  // SPI command should be {8âh13,cmd[7:0]}
  if (iAFE.POT4.iCHX_CFG.cmd_rcvd == 16'h1364)
    $display("Trigger level set successfully!");
  else
    $display("Trigger level NOT set correctly.");


  /////////////////////////////////////////
  // Set trigger positon to 511 (0x1FF) //
  ////////////////////////////////////////

  send_cmd = 1;
  cmd_snd = 24'h0401FF;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  if (iDUT.dc0.cm1.trig_pos == 9'h000)
    $display("Trigger position set successfully!");
  else
    $display("Trigger position NOT set correctly.");


    ////////////////////////
   // Set decimator to 1 //
  ////////////////////////

  send_cmd = 1;
  cmd_snd = 24'h050001;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  if (iDUT.dc0.descimator == 4'h1)
    $display("Decimator set successfully!");
  else
    $display("Decimator NOT set correctly.");


      /////////////////////////////////
     // Write trig_cfg              //
    // Pos edge, normal triggered  //
   // Channel 1 is source         //
  /////////////////////////////////

  send_cmd = 1;
  cmd_snd = 24'h060400;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  if (iDUT.dc0.trig_cfg == 8'h04 || iDUT.dc0.trig_cfg == 8'h24)  // capture_done bit may be set
    $display("trig_cfg set successfully!");
  else
    $display("trig_cfg NOT set correctly.");

    ///////////////////
   // Read trig_cfg //
  ///////////////////
	
	// TODO: See if done bit ever gets set
	while(resp_rcv != 8'h24) begin

    send_cmd = 1;
    cmd_snd = 24'h070000;

    @ (posedge clk); // wait a bit

    send_cmd = 0;

    @(posedge resp_rdy);

  	$display(resp_rcv);
	
	end

  // Positive acknowledgement is not sent back when reading trig_cfg
  if (resp_rcv == 8'h04 || resp_rcv == 8'h24)
    $display("Read trig_cfg successfully!");
  else
    $display("Read trig_cfg was a failure.");



 /************************************************
  *  Write calibration data to EEPROM 
  *  Gain is 0x80 (unity) for all three channels
  *  Offset is 0x00 for all three channels
  ************************************************/

    ////////////////////
   // Channel 1 gain //
  ////////////////////

  send_cmd = 1;
  cmd_snd = 24'h080B10;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  if (iEEP.mem[8'h0B] == 8'h10)  // 0x0B is the address of the gain term in EEP - check to make sure it's at unity (0x80)
    $display("Channel 1 gain wrote successfully!");
  else
    $display("Channel 1 gain NOT written correctly.");

    //////////////////////
   // Channel 1 offset //
  //////////////////////

  send_cmd = 1;
  cmd_snd = 24'h080A00;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  if (iEEP.mem[8'h0A] == 8'h00)
    $display("Channel 1 offset wrote successfully!");
  else
    $display("Channel 1 offset NOT written correctly.");


    ////////////////////
   // Channel 2 gain //
  ////////////////////

  send_cmd = 1;
  cmd_snd = 24'h081B20;
  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  if (iEEP.mem[8'h1B] == 8'h20)
    $display("Channel 2 gain wrote successfully!");
  else
    $display("Channel 2 gain NOT written correctly.");


    //////////////////////
   // Channel 2 offset //
  //////////////////////

  send_cmd = 1;
  cmd_snd = 24'h081A00;
  
  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);
  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  if (iEEP.mem[8'h1A] == 8'h00)
    $display("Channel 2 offset wrote successfully!");
  else
    $display("Channel 2 offset NOT written correctly.");


    ////////////////////
   // Channel 3 gain //
  ////////////////////

  send_cmd = 1;
  cmd_snd = 24'h082B30;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  if (iEEP.mem[8'h2B] == 8'h30)
    $display("Channel 3 gain wrote successfully!");
  else
    $display("Channel 3 gain NOT written correctly.");


    //////////////////////
   // Channel 3 offset //
  //////////////////////

  send_cmd = 1;
  cmd_snd = 24'h082A00;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  if (resp_rcv == 8'hA5)
    $display("Positive acknowledgement received");
  else
    $display("Negative acknowledgement (or no acknowledgement) received");

  if (iEEP.mem[8'h2A] == 8'h00)
    $display("Channel 3 offset wrote successfully!");
  else
    $display("Channel 3 offset NOT written correctly.");


    //////////////////////////////////
   // Read EEPROM - Channel 1 gain //
  //////////////////////////////////

  send_cmd = 1;
  cmd_snd = 24'h090B00;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  // Positive acknowledgement is not sent back when reading EEPROM
  if (resp_rcv == 8'h10)
    $display("Read Channel 1 gain successfully!");
  else
    $display("Reading Channel 1 gain was a failure.");

    //////////////////////////////////
   // Read EEPROM - Channel 2 gain //
  //////////////////////////////////

  send_cmd = 1;
  cmd_snd = 24'h091B00;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  // Positive acknowledgement is not sent back when reading EEPROM
  if (resp_rcv == 8'h20)
    $display("Read Channel 2 gain successfully!");
  else
    $display("Reading Channel 2 gain was a failure.");

    //////////////////////////////////
   // Read EEPROM - Channel 1 gain //
  //////////////////////////////////

  send_cmd = 1;
  cmd_snd = 24'h092B00;

  @ (posedge clk); // wait a bit

  send_cmd = 0;

  @(posedge resp_rdy);

  // Positive acknowledgement is not sent back when reading EEPROM
  if (resp_rcv == 8'h30)
    $display("Read Channel 3 gain successfully!");
  else
    $display("Reading Channel 3 gain was a failure.");

    $stop;

    ////////////
   /// DUMP ///
  ////////////

  send_cmd = 1;
  cmd_snd = 24'h010100;

  @ (posedge clk);

  send_cmd = 0;

  for(i = 0; i < 512; i = i + 1) begin
    @( posedge resp_rdy);
    mem_ch2[i] = resp_rcv;
  end

  send_cmd = 1;
  cmd_snd = 24'h010200;

  @(posedge clk);

  send_cmd = 0;

  for(i = 0; i < 512; i = i + 1) begin
    @( posedge resp_rdy);
    mem_ch3[i] = resp_rcv;
  end


  for(i = 2; i < 512; i = i + 1) begin
    $display(mem_ch2[i] - mem_ch3[i-2]);
  end

  $stop;

end
			 
endmodule
			 
			 