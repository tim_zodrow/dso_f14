module trigger_logic_tb();

reg clk, rst_n, trig1, trig2, trig_src, trig_edge, armed, trig_en, set_captured_done;
wire triggered;

trigger_logic TL(.clk(clk), .rst_n(rst_n), .trig1(trig1), .trig2(trig2), .trig_src(trig_src), .trig_edge(trig_edge), 
			.armed(armed), .trig_en(trig_en), .set_captured_done(set_captured_done), .triggered(triggered));

initial begin
	clk = 0;
	forever #5 clk = ~clk;
end

initial begin
	@(posedge clk)
		rst_n = 1'b0;
		trig_src = 1;
		trig_edge = 1;
		armed = 0;
		set_captured_done = 0;
		trig1 = 0;
		trig2 = 0;
		trig_en = 1;

	@(posedge clk)
		rst_n = 1'b1;
		armed = 1;
	#25;

	@(posedge clk)
		trig2 = 1;

	#200;
	$stop;
end

initial
	$monitor("%b t1 : %b t2 : %b t3 : %b \n", triggered, TL.trig_set_ff, TL.trig_sel_ff1, TL.trig_sel_ff2);
endmodule