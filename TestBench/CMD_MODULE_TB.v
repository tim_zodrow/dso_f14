module CMD_MODULE_TB();

// Input/Outputs //
///////////////////
reg stm_cmd_rdy, stm_resp_sent, stm_SPI_done, stm_clk, stm_rst_n, stm_rclk;
reg [7:0] stm_EEP_data;
reg [23:0] stm_cmd;
reg [7:0] stm_ch1_rdata, stm_ch2_rdata, stm_ch3_rdata;
reg [8:0] stm_trace_end;

reg [9:0] i;

wire wrt_SPI_mon, send_resp_mon, clr_cmd_rdy_mon, wrt_trig_pos_mon, wrt_trig_cfg_mon, wrt_deci_mon, send_trig_uart_mon;
wire [2:0] ss_mon;
wire [7:0] resp_data_mon;
wire [15:0] SPI_data_mon;
wire [8:0] ram_addr_mon;
// Output Registers
wire [8:0] trig_pos_mon;
wire [7:0] trig_cfg_mon;
wire [3:0] descimator_mon;

localparam DEFAULT_CMD = 24'h000000;
localparam POS_ACK = 8'hA5;
localparam NEG_ACK = 8'hEE;

cmd_module cm0(.clk(stm_clk), .rst_n(stm_rst_n), .cmd(stm_cmd), .cmd_rdy(stm_cmd_rdy), .wrt_SPI(wrt_SPI_mon), .SPI_done(stm_SPI_done), .ss(ss_mon), 
		.SPI_data(SPI_data_mon), .resp_data(resp_data_mon), .send_resp(send_resp_mon), .resp_sent(stm_resp_sent), .clr_cmd_rdy(clr_cmd_rdy_mon), 
		.EEP_data(stm_EEP_data), .ch1_rdata(stm_ch1_rdata), .ch2_rdata(stm_ch2_rdata), .ch3_rdata(stm_ch3_rdata), .trace_end(stm_trace_end), 
		.ram_addr(ram_addr_mon), .trig_pos(trig_pos_mon), .descimator(descimator_mon), .trig_cfg(trig_cfg_mon), .wrt_trig_pos(wrt_trig_pos_mon),
		.wrt_trig_cfg(wrt_trig_cfg_mon), .wrt_deci(wrt_deci_mon), .send_trig_uart(send_trig_uart_mon), .rclk(stm_rclk));

always
	#2 stm_clk <= ~stm_clk;

always
	#4 stm_rclk <= ~stm_rclk;

initial begin
	stm_cmd_rdy = 0;
	stm_resp_sent = 0;
	stm_SPI_done = 0;
	stm_clk = 0;
	stm_rst_n = 0;
	stm_rclk = 0;

	stm_EEP_data = 8'h00;
	stm_ch1_rdata = 8'h00;
	stm_ch2_rdata = 8'h00;
	stm_ch3_rdata = 8'h00;
	stm_trace_end = 9'h000;
	stm_cmd = DEFAULT_CMD;

	repeat(2) @ (posedge stm_clk);

	stm_rst_n = 1;

	//////////////////////////////
	// Ready to run after reset //
	//////////////////////////////

	repeat(2) @ (posedge stm_clk);

	///////////////////////////////////////
	// CFG Gain Set Channel 1  to 1 Volt //
	///////////////////////////////////////

	stm_cmd = 24'h021C00;
	stm_cmd_rdy = 1;

	trans_wait();

	stm_SPI_done = 1;
	@(posedge stm_clk);
	stm_SPI_done = 0;

	trans_wait();

	stm_resp_sent = 1;
	@(posedge stm_clk);
	stm_resp_sent = 0;

	stm_cmd = DEFAULT_CMD;
	stm_cmd_rdy = 0;

	//////////////////////
	// Between Commands //
	//////////////////////
	repeat(2) @ (posedge stm_clk);
	//////////////////////
	// Between Commands //
	//////////////////////

	///////////////////////////////////////
	// CFG Gain Set Channel 2  to 1 Volt //
	///////////////////////////////////////

	stm_cmd = 24'h021D00;
	stm_cmd_rdy = 1;

	trans_wait();

	stm_SPI_done = 1;
	@(posedge stm_clk);
	stm_SPI_done = 0;

	trans_wait();

	stm_resp_sent = 1;
	@(posedge stm_clk);
	stm_resp_sent = 0;

	stm_cmd = DEFAULT_CMD;
	stm_cmd_rdy = 0;

	//////////////////////
	// Between Commands //
	//////////////////////
	repeat(2) @ (posedge stm_clk);
	//////////////////////
	// Between Commands //
	//////////////////////

	///////////////////////////////////////
	// CFG Gain Set Channel 3  to 1 Volt //
	///////////////////////////////////////

	stm_cmd = 24'h021E00;
	stm_cmd_rdy = 1;

	trans_wait();

	stm_SPI_done = 1;
	@(posedge stm_clk);
	stm_SPI_done = 0;

	trans_wait();

	stm_resp_sent = 1;
	@(posedge stm_clk);
	stm_resp_sent = 0;

	stm_cmd = DEFAULT_CMD;
	stm_cmd_rdy = 0;


	//////////////////////
	// Between Commands //
	//////////////////////
	repeat(2) @ (posedge stm_clk);
	//////////////////////
	// Between Commands //
	//////////////////////

	stm_cmd = 24'h030000;
	stm_cmd_rdy = 1;

	trans_wait();

	stm_SPI_done = 1;
	@(posedge stm_clk);
	stm_SPI_done = 0;

	trans_wait();

	stm_resp_sent = 1;
	@(posedge stm_clk);
	stm_resp_sent = 0;

	stm_cmd = DEFAULT_CMD;
	stm_cmd_rdy = 0;


	//////////////////////
	// Between Commands //
	//////////////////////
	repeat(2) @ (posedge stm_clk);
	//////////////////////
	// Between Commands //
	//////////////////////

	stm_cmd = 24'h0300FF;
	stm_cmd_rdy = 1;

	trans_wait();

	stm_SPI_done = 1;
	@(posedge stm_clk);
	stm_SPI_done = 0;

	trans_wait();

	stm_resp_sent = 1;
	@(posedge stm_clk);
	stm_resp_sent = 0;

	stm_cmd = DEFAULT_CMD;
	stm_cmd_rdy = 0;


	//////////////////////
	// Between Commands //
	//////////////////////
	repeat(2) @ (posedge stm_clk);
	//////////////////////
	// Between Commands //
	//////////////////////

	stm_cmd = 24'h0300A4;
	stm_cmd_rdy = 1;

	trans_wait();

	stm_SPI_done = 1;
	@(posedge stm_clk);
	stm_SPI_done = 0;

	trans_wait();

	stm_resp_sent = 1;
	@(posedge stm_clk);
	stm_resp_sent = 0;

	stm_cmd = DEFAULT_CMD;
	stm_cmd_rdy = 0;


	//////////////////////
	// Between Commands //
	//////////////////////
	repeat(2) @ (posedge stm_clk);
	//////////////////////
	// Between Commands //
	//////////////////////

	stm_cmd = 24'h010000;
	stm_cmd_rdy = 1;
	stm_trace_end = 9'h001;

	trans_wait();

	stm_SPI_done = 1;
	@(posedge stm_clk);
	stm_SPI_done = 0;

	trans_wait();

	stm_SPI_done = 1;
	stm_EEP_data = 8'h01;
	@(posedge stm_clk);
	stm_SPI_done = 0;

	stm_SPI_done = 1;
	stm_EEP_data = 8'h80;
	stm_ch1_rdata = 8'h00;
	@(posedge stm_clk);
	stm_SPI_done = 0;

	for(i = 0; i < 512; i = i + 1) begin
		stm_ch1_rdata = i[7:0];
		trans_wait();
		stm_resp_sent = 1;
		@(posedge stm_clk);
		stm_resp_sent = 0;
	end

	stm_cmd = DEFAULT_CMD;
	stm_cmd_rdy = 0;

	$display("Well done, all tests passing!");
	$finish();
end

task trans_wait;
	repeat(8) @(posedge stm_clk);
endtask

endmodule