`timescale 1ns/1ps
module uart_transceiver_tb();

localparam TRANS_DATA = 8'h12;

reg stm_clk, stm_rst_n, stm_clr_rdy, stm_trmt;
reg [7:0] stm_tx_data;
wire rdy_mon, tx_done_mon, TX_RX;
wire [7:0] rx_data_mon;

// Implement Transceiver with connection between TX and RX using TX_RX
uart_transceiver ut0(.clk(stm_clk), .rst_n(stm_rst_n), .trmt(stm_trmt), .tx_data(stm_tx_data), .tx_done(tx_done_mon), .TX(TX_RX), .rdy(rdy_mon), .rx_data(rx_data_mon), .clr_rdy(stm_clr_rdy), .RX(TX_RX));

always
	#2 stm_clk <= ~stm_clk;

initial begin
	// Initial Values
	stm_clk = 1'b0;
	stm_rst_n = 1'b0;
	stm_clr_rdy = 1'b0;
	stm_trmt = 1'b1;
	stm_tx_data = TRANS_DATA;

	@(posedge stm_clk);
	@(negedge stm_clk);
	
	stm_rst_n = 1'b1; // Start Transaction

	@(posedge rdy_mon); // Wait for ready signal from RX

	if(rx_data_mon != TRANS_DATA) begin
		$display("ERROR: Incorrect data recieved");
		$stop();
	end
	
	$display("Well done!");
	$stop();
end

endmodule
