module cptr_module_tb();

reg clk, rst_n, rclk, trig1, trig2;
reg [8:0] trig_pos;
reg [7:0] trig_cfg;
reg [3:0] decim;

wire [8:0] trace_end, addr_ptr;
wire we, en, set_cptr_done;

cptr_module iDUT(.clk(clk), .rst_n(rst_n), .rclk(rclk), .trig1(trig1), .trig2(trig2), .trig_cfg(trig_cfg), 
		.trig_pos(trig_pos), .decim(decim), .trace_end(trace_end), .addr_ptr(addr_ptr), .we(we), .en(en), .set_cptr_done(set_cptr_done));

//inital values of inputs
initial begin
	clk = 0;
	rst_n = 1;
	rclk = 0;
	trig1 = 0;
	trig2 = 0;
	trig_pos = 9'h002; //take 2 samples
	trig_cfg = 8'h14; //pos_edge trig, normal trig, trig_src = channel 1 
	decim = 4'h2;
end

//clk period = 10
always begin
	#5 clk = ~clk;
end
//rclk twice as slow as clk
always begin
	#10 rclk = ~rclk;
end
//random trig periods
always begin
	#30 trig1 = ~trig1;
end
always begin 
	#40 trig2 = ~trig2;
end


initial begin
	rst_n = 0;    //reset module
	@(posedge clk) rst_n = 1;
	#100000;
	$stop;
end

endmodule
