module UART_comm_tb();

reg clk, rst_n, RX, clr_cmd_rdy, trmt, trmt_tx;
reg [7:0] tx_data, tx_data_tx; //tx_data_tx is transmitted data from transmitter not tranceiver

wire TX_c, TX, tx_done, tx_done_tx, cmd_rdy;
wire [23:0] cmd;

UART_comm uC(.clk(clk), .rst_n(rst_n), .RX(TX), .clr_cmd_rdy(clr_cmd_rdy), .trmt(trmt), 
								.tx_data(tx_data), .TX(TX_c),  .tx_done(tx_done), .cmd(cmd), .cmd_rdy(cmd_rdy));

UART_tx uTx(.clk(clk), .rst_n(rst_n), .tx_data(tx_data_tx), .trmt(trmt_tx), .TX(TX), .tx_done(tx_done_tx));

initial begin
	clk = 1'b0;
	rst_n = 1'b1;
	RX = 1'b1;
	clr_cmd_rdy = 1'b1;  //start with clearing cmd_rdy
	trmt = 1'b0;         //never transmit from tranceiver
	trmt_tx = 1'b0;      //use outside transmitter instead
	tx_data = 8'hxx;     //don't care about what tranceiver transmits
	tx_data_tx = 8'hDE;  //set data to send to comm module, high bits = 0xDE
end

always
	#5 clk = ~clk;

initial begin
	rst_n = 1'b0;
	@(posedge clk) rst_n = 1'b1; //reset module
	@(posedge clk) clr_cmd_rdy = 1'b0; //start processing commands
	
    //transmit byte
    @(posedge clk) trmt_tx = 1'b1; 
	@(posedge clk) trmt_tx = 1'b0;

	//when tx done
	@(posedge tx_done_tx);
	tx_data_tx = 8'hAD; //set mid bytes to 0xAD

	//transmit another byte
	@(posedge clk) trmt_tx = 1'b1;
	@(posedge clk) trmt_tx = 1'b0;

	//when tx done
	@(posedge tx_done_tx);
	tx_data_tx = 8'hED; //set low bytes to 0xED

	//transmit another byte
	@(posedge clk) trmt_tx = 1'b1;
	@(posedge clk) trmt_tx = 1'b0;
	
	@(posedge tx_done_tx); //wait until finished
    #500;

    if (cmd != 24'hDEADED)
		$display("FAILED: cmd should be 0xDEADED, instead is %h", cmd);
	else
		$display("SUCCESS: cmd is 0xDEADED");
	$stop;

end

endmodule
