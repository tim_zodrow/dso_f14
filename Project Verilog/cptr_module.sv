module cptr_module(clk, rst_n, rclk, trig1, trig2, trig_cfg, trig_pos, decim, trace_end, addr_ptr, we, en, set_cptr_done);

//*************************************VARIABLES*******************************************//

// Inputs
input clk, rst_n, rclk, trig1, trig2;
input [8:0] trig_pos;
input [7:0] trig_cfg;
input [3:0] decim;

// Output registers
output reg [8:0] trace_end, addr_ptr;
output reg we, en, set_cptr_done;


typedef enum reg [1:0] {WAIT_TRIG, SAMP1, SAMP2, DONE} state_t;
state_t state, nxt_state;

/////////////////////////////////////////////////////////////
// Registers - counts for samples, decimator, and trigger //
///////////////////////////////////////////////////////////
reg [15:0] dec_cnt;
reg [9:0] trig_cnt;
reg [15:0] dec_pwr;
reg [8:0] smpl_cnt;
reg [1:0] trig_type;

//triggered signal set from trigger_logic
reg triggered, keep_ff;

// Signals for SM
logic trig_en, trig_type_ld, set_trace_end, armed;
logic inc_addr, en_trig_cnt, en_smpl_cnt, en_dec_cnt; 
logic clr_trig_cnt, clr_smpl_cnt, clr_dec_cnt, keep;
//****************************************************************************************//

// Set armed when samples collected + trig_pos = 512
assign armed = &(smpl_cnt + trig_pos);
assign keep = (dec_cnt == dec_pwr) ? 1'b1 : 1'b0;
assign dec_pwr = 1<<(decim);

// Assign parts to for triggered logic
assign trig_en = |trig_cfg[3:2]; 

// Triggered reg set from this logic
trigger_logic tl0(.clk(clk), .rst_n(rst_n), .trig1(trig1), .trig2(trig2), .trig_src(trig_cfg[0]), .trig_edge(trig_cfg[4]), 
			.armed(armed), .trig_en(trig_en), .set_captured_done(trig_cfg[5]), .triggered(triggered));

///////////////////////////////
//Flop for switching addr_ptr//
///////////////////////////////
always_ff @(posedge clk, negedge rst_n) begin
  if(!rst_n)
    addr_ptr <= 9'h000;
  else if (inc_addr)
    addr_ptr <= addr_ptr + 1;  
end

/////////////////////
//Flop for trig_cnt//
/////////////////////
always_ff @(posedge clk, negedge rst_n) begin
  if(!rst_n)
		trig_cnt <= 9'h000;
  else if (clr_trig_cnt)
		trig_cnt <= 9'h000;
  else if (en_trig_cnt)
    trig_cnt <= trig_cnt + 1;
end

////////////////////
//Flop for dec_cnt//
////////////////////
always_ff @(posedge clk, negedge rst_n) begin
  if(!rst_n)
    dec_cnt <= 16'h0000;
  else if (clr_dec_cnt)
		dec_cnt <= 16'h0000;
  else if (en_dec_cnt)
   	dec_cnt <= dec_cnt + 1;
end

/////////////////
//Flop for keep//
/////////////////
always_ff @(posedge clk, negedge rst_n) begin
  if(!rst_n)
		keep_ff <= 1'b0;
  else
		keep_ff <= keep;
end

//////////////////
//Flop for state//
//////////////////
always_ff @(posedge clk, negedge rst_n) begin
  if(!rst_n)
    state <= WAIT_TRIG;
  else
    state <= nxt_state;
end

// Set trig_type
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		trig_type <= 2'b00;
	else if(trig_type_ld)
		trig_type <= trig_cfg[3:2];
end

// Set trace_end addr_ptr
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		trace_end <= 9'h000;
	else if(set_trace_end)
		trace_end <= addr_ptr;
end

// Increment smpl_cnt
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		smpl_cnt <= 9'h000;
	else if(en_smpl_cnt)
		smpl_cnt <= smpl_cnt + 1;
end

always_comb begin
	//default outputs
	clr_trig_cnt = 1'b0;
	clr_dec_cnt = 1'b0;
	clr_smpl_cnt = 1'b0;
	en_dec_cnt = 1'b0;
	en_trig_cnt = 1'b0;
	inc_addr = 1'b0;
	we = 0;
	en = 0;
	set_cptr_done = 0;
	nxt_state = WAIT_TRIG;
	trig_type_ld = 0;
	set_trace_end = 0;
	en_smpl_cnt = 0;

	case (state)
	WAIT_TRIG: begin
    	if (trig_en & ~rclk) begin
			//clear all counts
			clr_trig_cnt = 1'b1;
			clr_dec_cnt = 1'b1;
			trig_type_ld = 1;
			nxt_state = SAMP1;
		end
    end
    SAMP1: begin 
    	en_dec_cnt = 1; // decimator count enabled
    	inc_addr = keep_ff;
    	we = keep_ff;
    	en = keep_ff;
			nxt_state = SAMP2;
	end
	SAMP2: begin
		en_trig_cnt = (triggered | (trig_type[1] & armed)) & keep; //inc trig_cnt
		we = keep;
		en = keep;
		clr_dec_cnt = keep;
		en_smpl_cnt = keep;
		if(trig_cnt == trig_pos) begin
			set_cptr_done = 1;
			set_trace_end = 1; //save off last address
			nxt_state = DONE;
		end
		else
			nxt_state = SAMP1;    //not done, take another sample
	end
	DONE: begin
		if(trig_cfg[5]) //capture_done bit still set?
			nxt_state = DONE; //stay here
	end
  endcase
end

endmodule 
    

