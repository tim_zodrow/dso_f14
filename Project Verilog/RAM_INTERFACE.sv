module RAM_INTERFACE(clk, rst_n, RAM_en, rclk, dump_sel, dump_addr, capture_sel, capture_addr, RAM_addr);

////////////////////
// Input/Outputs //
//////////////////
input clk, rst_n, dump_sel, capture_sel;
input [8:0] dump_addr, capture_addr;

output RAM_en, rclk;
output reg [8:0] RAM_addr;

// rclk is 1/2 our system clock
reg [1:0] rclk_counter;

always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		RAM_addr <= 9'h000;
	// Capture or enable enabled
	else if(capture_sel)
		RAM_addr <= capture_addr;
	else if(dump_sel)
		RAM_addr <= dump_addr;
	else
		RAM_addr <= RAM_addr;
end

// rclk flop
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		rclk_counter <= 2'b00;
	else
		rclk_counter <= rclk_counter + 1;
end

// Assign rclk so it will be 1/2 system clk
assign rclk = rclk_counter[1];
// RAM needs to be enabled if capture or dump is selected
assign RAM_en = capture_sel | dump_sel; 

endmodule
