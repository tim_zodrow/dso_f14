// Trigger logic from in-class exercise 9
module trigger_logic(clk, rst_n, trig1, trig2, trig_src, trig_edge, armed, trig_en, set_captured_done, triggered);

////////////////////
// Input/Outputs //
//////////////////
input clk, rst_n, trig1, trig2, trig_src, trig_edge, armed, trig_en, set_captured_done;
output reg triggered;

wire and1, nor1, nor2, trig_sel, trig_set_en;

reg trig_set, trig_sel_ff1, trig_sel_ff2;

assign trig_sel = trig_src ? trig2 : trig1;

assign trig_set_en = trig_edge ? (trig_sel_ff1 & ~trig_sel_ff2) : (~trig_sel_ff1 & trig_sel_ff2);

// First and gate for trig_set, armed, and trig_en
assign and1 = trig_set & armed & trig_en;
assign nor1 = ~(and1 | triggered);
assign nor2 = ~(nor1 | set_captured_done);

always @ (*)
	if(triggered)
		trig_set <= 0;
	else if (trig_set_en)
		trig_set <= 1;

always @ (posedge clk, negedge rst_n) begin
	if(!rst_n) begin
		triggered <= 1'b0;
		trig_sel_ff1 <= 1'b0;
		trig_sel_ff2 <= 1'b0;
	end
	else begin
		triggered <= nor2;
		trig_sel_ff1 <= trig_sel;
		trig_sel_ff2 <= trig_sel_ff1;
	end
end

endmodule
