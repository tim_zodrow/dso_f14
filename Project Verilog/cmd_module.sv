module cmd_module(clk, rst_n, cmd, cmd_rdy, wrt_SPI, SPI_done, ss, SPI_data, resp_data, send_resp, 
				resp_sent, clr_cmd_rdy, EEP_data, ch1_rdata, ch2_rdata, ch3_rdata, trace_end, 
				ram_addr, trig_pos, descimator, trig_cfg, wrt_trig_pos, wrt_trig_cfg, wrt_deci, 
				send_trig_uart, rclk, cmd_sel);
///////////////////
// Input/Outputs //
///{////////////////
   input clk, rst_n, cmd_rdy, resp_sent, SPI_done, rclk;
   input [7:0] EEP_data;
   input [23:0] cmd;
   input [7:0] ch1_rdata, ch2_rdata, ch3_rdata;
   input [8:0] trace_end;

// Output Registers
output reg wrt_SPI, send_resp, clr_cmd_rdy, wrt_trig_pos, wrt_trig_cfg, wrt_deci, send_trig_uart, cmd_sel;
output reg [2:0] ss;
output reg [7:0] resp_data;
output reg [15:0] SPI_data;
output reg [8:0] ram_addr;
output [8:0] trig_pos;
output [7:0] trig_cfg;
output [3:0] descimator;

//////////////////////
// Acknowledgements //
//////////////////////
localparam POS_ACK = 8'hA5;
localparam NEG_ACK = 8'hEE;
///////////////
// Registers //
///////////////
reg [2:0] ch1_gain, ch2_gain, ch3_gain; // Channel Gain Registers
reg [1:0] channel;  // Selected channel
reg [7:0] offset, gain; // Values of offset and gain read from EEP
reg [8:0] addr_ptr;
reg rclk_ff1;
///////////////////
// Command Codes //
///////////////////
typedef enum reg [4:0] {BAD_COMMAND, DUMP, CFG_GAIN, SET_TRIG, WRT_TRIG_POS, SET_DECI, WRT_TRIG_CFG, 
				RD_TRIG_CFG, WRT_EEP, RD_EEP} cmd_t;

////////////
// States //
//////////// 
typedef enum reg [4:0] {CMD_DISPATCH, RD_EEP2, RD_EEP3, WAIT_SPI, DUMP_READ_OFFSET, 
				DUMP_READ_START, DUMP_READ_JUNK, DUMP_READ_GAIN, DUMP_WRITE_UART, 
				DUMP_SEND_UART, WRITE_UART, RAM_WAIT} state_t;
state_t state, nxt_state;

/////////////////////
// Wires and Logic //
/////////////////////
logic read_offset, read_gain, capture_ch1_gain, capture_ch2_gain, capture_ch3_gain, addr_load, 
		addr_inc, enable_channel, wrt_ss;
logic [2:0] ss_channel;
wire [7:0] raw, corrected;

///////////
// Flops //
///////////
// State Machine Flop
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		state <= CMD_DISPATCH;
	else
		state <= nxt_state;
end

// Gain channel 1
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		ch1_gain <= 0;
	else if(capture_ch1_gain)
		ch1_gain <= cmd[12:10];
end

// Gain channel 2
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		ch2_gain <= 0;
	else if(capture_ch2_gain)
		ch2_gain <= cmd[12:10];
end

// Gain channel 3
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		ch3_gain <= 0;
	else if(capture_ch3_gain)
		ch3_gain <= cmd[12:10];
end

// Hold channel selection
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n)
		channel <= 2'b00;
	else if(enable_channel)
		channel <= cmd[9:8];
end

// Read offset from EEP
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n) begin
		offset <= 8'h00;
	end
	else if(read_offset) begin
		offset <= EEP_data;
	end
end

// Read gain from EEP
always_ff @ (posedge clk, negedge rst_n) begin
	if(!rst_n) begin
		gain <= 8'h00;
	end
	else if(read_gain) begin
		gain <= EEP_data;
	end
end

// Increment and load addr_ptr
always_ff @(posedge clk, negedge rst_n) begin
	if(!rst_n) begin
		addr_ptr <= 8'h00;
	end
	else if(addr_load) begin
		addr_ptr <= (trace_end+1);
	end
	else if(addr_inc) begin
		addr_ptr <= (addr_ptr+1);
	end
end

// ss_channel flop
always_ff @(posedge clk, negedge rst_n) begin
	if(!rst_n)
		ss <= 3'b000;
	else if(wrt_ss)
		ss <= ss_channel;
end

// rclk flop
always_ff @(posedge clk, negedge rst_n) begin
	if(!rst_n)
		rclk_ff1 <= 0;
	else
		rclk_ff1 <= rclk;
end

//////////////////
// Assignments //
/////////////////
assign raw = (~channel[0] & ~channel[1]) ? ch1_rdata :
	     (channel[0] & ~channel[1]) ? ch2_rdata : ch3_rdata;
assign ram_addr = addr_ptr;
assign trig_pos = cmd[8:0];
assign trig_cfg = cmd[15:8];
assign descimator = cmd[3:0];

// Gain correction logic
raw2corrected r2c0(.raw(raw), .offset(offset), .gain(gain), .corrected(corrected));

///////////////////
// State Machine //
///////////////////
always_comb begin
	// Default Values
	capture_ch1_gain = 0;
	capture_ch2_gain = 0;
	capture_ch3_gain = 0;
	enable_channel = 0;
	addr_load = 0;
	addr_inc = 0;
	clr_cmd_rdy = 0;
	wrt_SPI = 0;
	wrt_deci = 0;
	wrt_trig_cfg = 0;
	wrt_trig_pos = 0;
	send_resp = 0;
	nxt_state = CMD_DISPATCH;
	send_trig_uart = 0;
	SPI_data = 16'h0000;
	read_offset = 0;
	read_gain = 0;
	resp_data = 8'h00;
	wrt_ss = 0;
	ss_channel = 3'b000;
	cmd_sel = 0;

	case (state)
		CMD_DISPATCH : begin
			if(cmd_rdy) begin
				case(cmd[19:16])
					// Dump RAM Channel
					DUMP : begin
						ss_channel = 3'b100; // select EEPROM
						wrt_ss = 1;
						addr_load = 1;
						enable_channel = 1;
						nxt_state = DUMP_READ_START;
					end
					// Set CFG Gain
					CFG_GAIN : begin
						wrt_SPI = 1;
						wrt_ss = 1;
						// Determine which channel to set
						case(cmd[9:8])
							2'b00 : begin
								ss_channel = 3'b001;
								capture_ch1_gain = 1;
							end
							2'b01 : begin
								ss_channel = 3'b010;
								capture_ch2_gain = 1;
							end
							2'b10 : begin
								ss_channel = 3'b011;
								capture_ch3_gain = 1;
							end
							default : begin
								ss_channel = 3'b111; // BAD CHANNEL
							end
						endcase
						// Interpret Gain Settings Value
						case (cmd[12:10])
							3'b000 : SPI_data = 16'h1302;
							3'b001 : SPI_data = 16'h1305;
							3'b010 : SPI_data = 16'h1309;
							3'b011 : SPI_data = 16'h1314;
							3'b100 : SPI_data = 16'h1328;
							3'b101 : SPI_data = 16'h1346;
							3'b110 : SPI_data = 16'h136B;
							3'b111 : SPI_data = 16'h13DD;
						endcase
						nxt_state = WAIT_SPI;
					end
					// Set trigger level
					SET_TRIG : begin
						wrt_SPI = 1;
						wrt_ss = 1;
						ss_channel = 3'b000;
						// Saturation of values if not in the range of 46 to 201
						SPI_data = (cmd[7:0] < 8'h2E) ? {16'h132E} : 
							   (cmd[7:0] > 8'hC9) ? {16'h13C9} : {8'h13, cmd[7:0]};
						nxt_state = WAIT_SPI;
					end
					// Write trigger position register
					WRT_TRIG_POS : begin
						wrt_trig_pos = 1; // Capture cmd[8:0]
						send_resp = 1;
						resp_data = POS_ACK;
						nxt_state = WRITE_UART;
					end
					// Write descimator register
					SET_DECI : begin
						wrt_deci = 1;
						send_resp = 1;
						resp_data = POS_ACK;
						nxt_state = WRITE_UART;
					end
					WRT_TRIG_CFG : begin
						wrt_trig_cfg = 1;
						send_resp = 1;
						resp_data = POS_ACK;
						nxt_state = WRITE_UART;
					end
					RD_TRIG_CFG : begin
						send_resp = 1;
						send_trig_uart = 1;
						nxt_state = WRITE_UART;
					end
					// Write to SPI
					WRT_EEP : begin
						wrt_SPI = 1;
						wrt_ss = 1;
						ss_channel = 3'b100;
						SPI_data = {2'b01,cmd[13:0]};
						nxt_state = WAIT_SPI;
					end
					// Read from SPI
					RD_EEP : begin
						wrt_SPI = 1;
						wrt_ss = 1;
						ss_channel = 3'b100;
						SPI_data = {2'b00,cmd[13:0]};
						nxt_state = RD_EEP2;
					end
					// Default, bad command
					default : begin
						send_resp = 1;
						resp_data = NEG_ACK;
						nxt_state = WRITE_UART;
					end
				endcase
			end
		end
		// Second read from EEP
		RD_EEP2 : begin
			if(~SPI_done) begin
				nxt_state = RD_EEP2;
			end
			else begin
				wrt_SPI = 1;
				SPI_data = {2'b00,cmd[13:0]}; // Junk Data for Read
				nxt_state = RD_EEP3;
			end
		end
		// Third read from EEP
		RD_EEP3 : begin
			if(~SPI_done) begin
				nxt_state = RD_EEP3;
			end
			else begin
				send_resp = 1;
				resp_data = EEP_data;
				nxt_state = WRITE_UART;
			end
		end
		// Wait for SPI to finish
		WAIT_SPI : begin
			if (~SPI_done) begin
				nxt_state = WAIT_SPI;
			end
			else begin
				send_resp = 1;
				nxt_state = WRITE_UART;
				resp_data = POS_ACK;
			end
		end
		// Prepare to read for the start of the dump cycle
		DUMP_READ_START : begin
			wrt_SPI = 1;
			case(channel)
				2'b00 : begin
					SPI_data = {4'h0, ch1_gain, 9'h0};
				end
				2'b01 : begin
					SPI_data = {4'h1, ch2_gain, 9'h0};
				end
				2'b10 : begin
					SPI_data = {4'h2, ch3_gain, 9'h0};
				end
			endcase
			nxt_state = DUMP_READ_JUNK;
		end
		// Next read cycle of junk in dump cycle
		DUMP_READ_JUNK : begin
			if (SPI_done) begin
				nxt_state = DUMP_READ_OFFSET;
				wrt_SPI = 1;
				case(channel)
					2'b00 : begin
						SPI_data = {4'h0, ch1_gain, 1'b1, 8'h0};
					end
					2'b01 : begin
						SPI_data = {4'h1, ch2_gain, 1'b1, 8'h0};
					end
					2'b10 : begin
						SPI_data = {4'h2, ch3_gain, 1'b1, 8'h0};
					end
				endcase
			end
			else begin
				nxt_state = DUMP_READ_JUNK;
			end
		end
		// Prepare to read offset
		DUMP_READ_OFFSET : begin
			if (SPI_done) begin
				read_offset = 1; // Need to read offset from EEPROM next
				wrt_SPI = 1;
				nxt_state = DUMP_READ_GAIN;
				SPI_data = 16'h0000; // Pass in read command with junk data
			end
			else begin
				nxt_state = DUMP_READ_OFFSET;
			end
		end
		// Prepare to read gain
		DUMP_READ_GAIN: begin
			if (SPI_done) begin
				nxt_state = RAM_WAIT;
				read_gain = 1;
			end
			else begin
				cmd_sel = 1;
				nxt_state = DUMP_READ_GAIN;
			end
		end
		// Send to UART
		DUMP_SEND_UART : begin
			cmd_sel = 1;
			send_resp = 1;
			resp_data = corrected;
			nxt_state = DUMP_WRITE_UART;
		end
		// Start writing out to UART
		DUMP_WRITE_UART : begin
			if (resp_sent) begin
				if (addr_ptr != trace_end) begin
					addr_inc = 1;
					nxt_state = RAM_WAIT;
				end
				else begin
					clr_cmd_rdy = 1;
				end
			end
			else begin
				nxt_state = DUMP_WRITE_UART;
			end
		end
		// Wait for Posedge rclk for new address
		RAM_WAIT : begin
			if(rclk & ~rclk_ff1) begin
				nxt_state = DUMP_SEND_UART;
				cmd_sel = 1;
			end
			else
				nxt_state = RAM_WAIT;
		end
		// Write to UART while resp is not sent
		WRITE_UART : begin
			if(~resp_sent) begin
				nxt_state = WRITE_UART;
			end
			else begin
				clr_cmd_rdy = 1;
			end
		end

		default : begin
			// Reset to defaults
		end
	endcase
end

endmodule
				

