// Module taken from HW2 Solution
// Gain correction logic
module raw2corrected(raw, offset, gain, corrected);

////////////////////
// Input/Outputs //
//////////////////
input [7:0] raw;
input [7:0] offset;
input [7:0] gain;

output [7:0] corrected;

wire [7:0] sum, sat_sum;
wire [15:0] prod, sat_prod;

assign sum = raw + offset;

assign sat_sum = (~offset[7] && raw[7] && ~sum[7]) ? 8'hFF :
		 (offset[7] && ~raw[7] && sum[7]) ? 8'h00 : sum;

assign prod = sat_sum * gain;

assign sat_prod = (prod[15]) ? 16'h7FFF : prod;

assign corrected = sat_prod[14:7];

endmodule
